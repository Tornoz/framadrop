POT=lib/Lufi/I18N/framadrop.pot
SEDOPTS=-e "s@SOME DESCRIPTIVE TITLE@Lufi language file@" \
		-e "s@YEAR THE PACKAGE'S COPYRIGHT HOLDER@2015 Luc Didry@" \
		-e "s@CHARSET@utf8@" \
		-e "s@the PACKAGE package@the Lufi package@" \
		-e '/^\#\. (/{N;/\n\#\. (/{N;/\n.*\.\.\/default\//{s/\#\..*\n.*\#\./\#. (/g}}}' \
		-e '/^\#\. (/{N;/\n.*\.\.\/default\//{s/\n/ /}}' 
SEDOPTS2=-e '/^\#.*\.\.\/default\//,+3d'
XGETTEXT=carton exec ../../local/bin/xgettext.pl

locales:
		$(XGETTEXT) -D templates -D ../default/templates -o $(POT) 2>/dev/null
		sed $(SEDOPTS) -i $(POT)
		sed $(SEDOPTS2) -i $(POT)

push-locales:
	zanata-cli -q -B push --errors --project-version `git branch | grep \* | cut -d ' ' -f2-`

pull-locales:
	zanata-cli -q -B pull --project-version `git branch | grep \* | cut -d ' ' -f2-`

stats-locales:
	zanata-cli -q stats --project-version `git branch | grep \* | cut -d ' ' -f2-`
