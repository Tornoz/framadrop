% # vim:set sw=4 ts=4 sts=4 ft=html.epl expandtab:
% my %d = (
%   delay_0   => l('no time limit'),
%   delay_1   => l('24 hours'),
%   delay_365 => l('1 year')
% );
    <div id="results">
        <h2><%= l('Uploaded files') %></h2>
        <div id="misc" class="form-group"></div>
        <ul class="list-group" id="ul-results">
        </ul>
    </div>

    <div class="form-group form-inline">
        <noscript>
            <div class="col-sm-12 col-xs-12">
                <div class="alert alert-danger">
                    <strong><%= l('Javascript is disabled. You won\'t be able to use Lufi.') %></strong>
                </div>
            </div>
        </noscript>
% if (defined(config('broadcast_message'))) {
        <div class="col-sm-12 col-xs-12">
            <div class="alert alert-info">
                <strong><%= config('broadcast_message') %></strong>
            </div>
        </div>
% }
% if (stop_upload) {
        <div class="col-sm-12 col-xs-12">
            <div class="alert alert-danger">
                <strong><%= l('Sorry, the uploading is currently disabled. Please try again later.') %></strong>
            </div>
        </div>
% } else {
        <div class="btn btn-default" id="expiration">
            <label for="delete-day"><%= l('Expiration') %></label>
            <select id="delete-day" class="">
% for my $delay (qw/0 1 7 30 365/) {
%   my $text = ($delay == 7 || $delay == 30) ? l('%1 days', $delay) : $d{'delay_'.$delay};
%   if (max_delay) {
%       if ($delay) {
%           if ($delay < max_delay) {
                    <option value="<%= $delay %>" <%== is_selected($delay) %>><%= $text %></option>
%           } elsif ($delay == max_delay) {
                    <option value="<%= $delay %>" <%== is_selected($delay) %>><%= $text %></option>
%               last;
%           } else {
%               my $text = ($delay == 1) ? l('24 hours') : l('%1 days', $delay);
                    <option value="<%= max_delay %>" <%== is_selected(max_delay) %>><%= l('%1 days', max_delay) %></option>
%               last;
%           }
%       }
%   } else {
                    <option value="<%= $delay %>" <%== is_selected($delay) %>><%= $text %></option>
%   }
% }
            </select>
            <a href="<%= url_for('/delays') %>" title="<%= l('Important: more information on delays') %>"><span class="fa fa-fw fa-lg  fa-info-circle"></span></a>
        </div>
        <div class="btn btn-default" id="burnafterreading">
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="first-view"> <%= l('Delete at first download?') %>
                </label>
            </div>
        </div>
    </div>
    <div id="files" class="tab-content">
        <div id="drag-and-drop-zone" class="uploader well">
            <p class="h2"><%= l('Drop files here') %></p>
            <p class="or"><%= l('or') %></p>
            <label class="btn btn-primary btn-xs">
                <span><%= l('Click to open the file browser') %></span>
                <input class="sr-only" type="file" onchange="handleFiles(this.files)" multiple>
            </label>
        </div>
    </div>

%= javascript begin
var ws_url  = '<%= url_for('upload')->to_abs() %>';
var baseURL = '<%= url_for('/')->to_abs() %>';
var i18n    = {
    confirmExit: '<%= l('You have attempted to leave this page. The upload will be canceled. Are you sure?') %>',
    copyAll:     '<%= l('Copy all links to clipboard') %>',
    copySuccess: '<%= l('The link(s) has been copied to your clipboard') %>',
    copyFail:    '<%= l('Unable to copy the link(s) to your clipboard') %>',
    cpText:      '<%= l('Copy to clipboard') %>',
    delText:     '<%= l('Deletion link') %>',
    dlText:      '<%= l('Download link') %>',
    download:    '<%= l('Download') %>',
    encrypting:  '<%= l('Encrypting part XX1 of XX2') %>',
    expiration:  '<%= l('Expiration:') %>',
    hit:         '<%= l('Hit Enter, then Ctrl+C to copy the download link') %>',
    hits:        '<%= l('Hit Enter, then Ctrl+C to copy all the download links') %>',
    mailTo:      '<%= l('Send all links by email') %>',
    noLimit:     '<%= l('No expiration delay') %>',
    sending:     '<%= l('Sending part XX1 of XX2. Please, be patient, the progress bar can take a while to move.') %>',
    wsProblem:   '<%= l('Websocket communication error') %>',
};
% end
%= javascript '/js/sjcl.js'
%= javascript '/js/moment-with-locales.min.js'
%= javascript '/js/lufi-up.js'
% }
